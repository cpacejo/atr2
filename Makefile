PASC = fpc
PASFLAGS = -O2

.PHONY: all

all: atr2 atrt

atr2 atrt: %: %.pas
	$(PASC) $(PASFLAGS) $<

%.ppu %.o &: %.pas
	$(PASC) $(PASFLAGS) -E $<

atr2: atr2func.ppu egavga.ppu filelib.ppu myfile.ppu
atrt: atr2func.ppu filelib.ppu myfile.ppu
myfile.o: filelib.ppu
atr2func.o: filelib.ppu myfile.ppu
